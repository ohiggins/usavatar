<?php 
	require_once '../init.php';
	require_once $abs_us_root.$us_url_root.'users/includes/header.php';
	require_once $abs_us_root.$us_url_root.'users/includes/navigation.php';
?>

<div id="page-wrapper">
<div class="container">
<div class="well">
<div class="row">
	<div class="col-xs-12 col-md-3">
<div style="text-align: center; margin-top: 50px">
    <form role="form" id="avatarForm" method="post" enctype="multipart/form-data">
        <input type="hidden" name="code" value="DSAFDSGFDGGFHFJ45245345^GFDGGSDFGDSDSFFHHQACFBHJGPOIUJ">
        <label class="avatar" for="uploadAvatar" style="display: inline-block; position: relative; overflow: hidden; width: 120px; height: 120px; border-radius: 50%; background-color: #f6f6f6; cursor: pointer">
            <span class="embed-avatar" style="display: block; width: 100%; height: 100%; display: block; position: absolute; top: 0; left: 0;">
                <span class="img-avatar img-avatar-lg bg" style="display: block; width: 100%; height: 100%; background: url('<?='images/'.$user->data()->id.".jpg"; ?>') no-repeat center; background-size: 100% 100%;"></span>
            </span>
            <span class="camera" style="width: 100%;height: 100%;z-index: 9999;text-align: center;position: absolute;top: 0;left: 0;background: #F0F0F0;opacity: 0.5;padding: 0;padding-top: 43%;">
                <i class="fa fa-camera" style="color: blueviolet"></i>
            </span>
            <input type="file" name="uploadAvatar" id="uploadAvatar" class="upload-avatar" accept="image/*" style="display: none;">
        </label>	
    </form>
	<p><a href="../../users/account.php" class="btn btn-primary">Account Info</a></p>
</div>
</div>
</div>
</div>
</div>
</div>




<script>
    $(function () {
        $("input[name='uploadAvatar']").change(function () {
            if (this.files.length > 0) {
                if (!window.FormData) {
                    alert("Your browse does not support FormData object.");
                    return false;
                }
                //console.log(this.form); return;

                $.ajax({
                    url: "ajax/avatar-upload.php",
                    method: 'POST',
                    dataType: 'json',
                    data: new FormData(this.form),
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        //
                    },
                    success: function (data) {
                        if (data['status'] === 'ok') {
                            var uploaded_url = data['uploaded_url'];
                            $(".avatar .embed-avatar .img-avatar").css({"background-image": "url('" + uploaded_url + "')"});
                        }
                    },
                    complete: function () {
                        //
                    },
                    error: function () {
                        //
                    }
                });
            }
        });
    });
</script>

<?php require_once $abs_us_root.$us_url_root.'users/includes/page_footer.php'; // the final html footer copyright row + the external js calls ?>

<!-- Place any per-page javascript here -->

<?php require_once $abs_us_root.$us_url_root.'users/includes/html_footer.php'; // currently just the closing /body and /html ?>
