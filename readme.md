# Avatar upload for Userspice with ajax
PHP upload avatar image with jQuery ajax post request

##How to install
Drop the avatar dirctory into the users directory. You will now be able to point the any user to the avatar/index.php and they will upload a new avatar. The system does not store more than one image at a time per user.

	users/avatar/images/default-avatar.png
	users/avatar/ajax/avatar-upload.php
	users/avatar/config.php
	users/avatar/index.php

You will need to add the following file into usersc directoy if you want the generic files changed.  These are only there to seperate the 2 files as there are templating changes that occur between versions. Simply pick your version and drop the account.php file into usersc.

    /usersc/account.php
    /usersc/profile.php
    /usersc/view_all.php
    

There are changes between the differnt versions and there is a file for each. If you have already modded all you need to do is find the following line
    <p><img src="<?=$grav; ?>" class="img-thumbnail" alt="Generic placeholder thumbnail"></p>
and replace with
    <img src="<?='../users/avatar/images/'.$user->data()->id.".jpg"; ?>" class="img-thumbnail" onerror="this.src='../users/avatar/images/default-avatar.png'" alt="<?php echo $user->data()->fname." ".$user->data()->lname ?>">

To access the avatar just use;

    <?php
    
    <img src="<?='../users/avatar/images/'.$user->data()->id.".jpg"; ?>" class="img-thumbnail" onerror="this.src='../users/avatar/images/default-avatar.png'" alt="<?php echo $user->data()->fname." ".$user->data()->lname ?>">
    
    ?>

At any point you want to to appear, just use the above code. Note that it does use the img-thumbnail class so if you want ht eimage directly then just use;

    <?php
    
    <img src="<?='../users/avatar/images/'.$user->data()->id.".jpg"; ?>">
    
    ?>
##Usage
- With all th files in place navigate to the user account page. You will find an extra buttin there saying "Edit Avatar". Click on this and you will be taken to the uplaod screen. Click the camera and selcet a jpg file. The file will upload before showing you a quick preview. Now click the "Account info" button and you will reutn to the account summary page.

##Notes
- The user must be logged in for this to work.
- This version only supports images with the *.jpg extension.
- With the current code it works best if you upload a square or 1:1 ratio image.
- If the image is not sqaure or 1:1 then its best used with the class="img-thumbnail" as this will constrain the proportions.
- Please do a hard refresh or empty the cache if nothing changes.
- If you upload something other then a jpg it simply will not chnage. Check the file you are uploading.
- The first time a user logs in they will have no associtaed image and the missing image will show on any pages that you are calling other than account.php. To fix this add the below code to the <img> tag but make sure you correct the url paths
    onerror="this.src='../users/avatar/images/default-avatar.png'"
    
##Issues
- You my find that you have permission issues. This was the case on an Unbuntu install that was used for testing butnot an issue on windows. The following worked during testing;


        sudo chown -R www-data:www-data /var/www/html/


##Tested on
- 4.3.25
<<<<<<< HEAD
- 4.4 Beta (works but the template engine is differnt so you will need to modify the /usersc files.)
=======
- 4.4 Beta (works but the template engine is different so you will need to modify the /usersc files.)
>>>>>>> ff7cf81b3fe62cb088af33b83994d567dcea2b90
